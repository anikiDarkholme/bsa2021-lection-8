import { createElement } from './helpers/domHelper'; 
import { FighterInfo } from './models/FighterInfo';

export function createFighter(fighter: FighterInfo,
     handleClick : (ev: Event) => any,
    selectFighter : (ev: Event) => any)
    : HTMLElement {
  const { name, source } = fighter;
  const nameElement: HTMLElement =
   createName(name);

  const imageElement: HTMLElement =
   createImage(source);

  const checkboxElement: HTMLElement =
   createCheckbox();

  const fighterContainer: HTMLElement =
   createElement({ tagName: 'div', className: 'fighter' });
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: MouseEvent) => ev.stopPropagation();
  const onCheckboxClick = selectFighter;
  const onFighterClick = handleClick;

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

function createName(name: string): HTMLElement {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: string): HTMLElement {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  return imgElement;
}

function createCheckbox(): HTMLElement {
  const label = createElement({ tagName: 'label', className: 'custom-checkbox' });
  const span = createElement({ tagName: 'span', className: 'checkmark' });
  const attributes = { type: 'checkbox' };
  const checkboxElement = createElement({ tagName: 'input', attributes });

  label.append(checkboxElement, span);
  return label;
}