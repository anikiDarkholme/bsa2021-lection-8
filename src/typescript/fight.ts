import { createElement } from "./helpers/domHelper";
import { showFightScene } from "./modals/fightScene";
import { showModal } from "./modals/modal";
import { showWinnerModal } from "./modals/winner";
import { FullFighterInfo } from "./models/FullFighterInfo";

var leftOpponent: FullFighterInfo = new FullFighterInfo();
var rightOpponent: FullFighterInfo = new FullFighterInfo();
var timeoutTime : number = 1000;
export function fight(firstFighter: FullFighterInfo, secondFighter: FullFighterInfo)
:void {
     timeoutTime = 1000;

     Object.assign(leftOpponent,firstFighter);
     Object.assign(rightOpponent,secondFighter);
     
      showFightScene(leftOpponent, rightOpponent);

      let speedUpBtn = createSpeedUp();

      setTimeout(() => 
      {
        round(leftOpponent, rightOpponent);
        document.body.append(speedUpBtn);
      }, timeoutTime + 500);

  }
  
function createSpeedUp() {
  let speedUpBtn = createElement({ tagName: "button", className: "speed-up" });

  speedUpBtn.innerText = "»";

  speedUpBtn.addEventListener('click', () => {
    timeoutTime = 200;

    Array.from(
      document.getElementsByClassName("speed-up")).forEach(element => { element.remove(); });
  });
  return speedUpBtn;
}

  function round(leftOpponent: FullFighterInfo, rightOpponent: FullFighterInfo): 
  FullFighterInfo | void
 {  
    if(!tryCleanField(leftOpponent, rightOpponent))
        return;
 
    let damage = getDamage(leftOpponent, rightOpponent);

    let infoElement: HTMLElement = createElement({tagName : "div" , className: "info-element"});

    infoElement.innerHTML =
     `${leftOpponent.name} hits ${rightOpponent.name} by ${damage} <br>
      Current health points: <br>
       ${leftOpponent.name} : ${leftOpponent.health} |
       ${rightOpponent.name} : ${rightOpponent.health-= damage}`;

    showModal({
        title:`After round info:`, 
        bodyElement: infoElement});

          setTimeout( () => round(rightOpponent, leftOpponent), timeoutTime);
  }

  function tryCleanField(leftOpponent: FullFighterInfo, rightOpponent: FullFighterInfo)
  :boolean
  {
    if(leftOpponent.health <= 0 ){
      for (const iterator of document.getElementsByClassName("modal-layer")) {
        iterator.remove();
      }
      showWinnerModal(rightOpponent);
      return false;
    }
    if(rightOpponent.health <= 0){
      for (const iterator of document.getElementsByClassName("modal-layer")) {
        iterator.remove();
      }
      showWinnerModal(rightOpponent);
      return false;
    }      
    
    for (const iterator of document.getElementsByClassName("modal-layer")) {
      if(! Array.from(iterator.children[0].children)
        .some(n => n.className.includes("battlefield")))
      iterator.remove();
    }

    return true;
  }

  export function getDamage(attacker: FullFighterInfo, enemy: FullFighterInfo)
  :number {
      let damage: number = getHitPower(attacker) - getBlockPower(enemy);

      return damage > 0? damage: 0;
  }
  
  export function getHitPower(fighter: FullFighterInfo)
  :number {
    let hitPower: number = fighter.attack * (Math.random() + 1);

    return hitPower
  }
  
  export function getBlockPower(fighter: FullFighterInfo)
  :number {
    let blockPower: number = fighter.defense * (Math.random() + 1);

    return blockPower
  }
  