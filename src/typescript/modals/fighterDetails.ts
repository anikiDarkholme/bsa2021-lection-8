import { createElement } from '../helpers/domHelper';
import { FullFighterInfo } from '../models/FullFighterInfo';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter: FullFighterInfo) {
  const title: string =
    'Fighter info';
   
  const bodyElement: HTMLElement = 
    createFighterDetails(fighter);

  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FullFighterInfo) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  
  nameElement.innerHTML = ` <strong>Name:</strong> ${name} <br>`+
                            ` <strong>Attack:</strong> ${attack} <br>`+
                            ` <strong>Defense:</strong> ${defense} <br>`+
                            ` <strong>Health:</strong> ${health} <br>`+
                            ` <img src = '${source}'/>`;
  fighterDetails.append(nameElement);

  return fighterDetails;
}
