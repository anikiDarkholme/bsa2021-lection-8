import { createFighter } from "../fighterView";
import { FighterInfo } from "../models/FighterInfo";
import { showModal } from "./modal";

export  function showWinnerModal(fighter: FighterInfo) {
  let fighterElement = createFighter(
    fighter,
    (ev:Event) => ev.stopPropagation(),
    (ev:Event) => ev.stopPropagation()
  );

   showModal({title: `Winner : `, bodyElement: fighterElement});
  }