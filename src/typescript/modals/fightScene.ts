import { createFighter } from "../fighterView";
import { createElement } from "../helpers/domHelper";
import { FullFighterInfo } from "../models/FullFighterInfo";
import { showModal } from "./modal";

export function showFightScene(leftOpponent: FullFighterInfo, rightOpponent: FullFighterInfo)
{
    let battlefield =
        createBattlefield(leftOpponent,rightOpponent);

    showModal({
        title:`Find between ${leftOpponent.name} and ${rightOpponent.name}`, 
        bodyElement: battlefield});
}

function createBattlefield(leftOpponent: FullFighterInfo, rightOpponent: FullFighterInfo)
:HTMLElement
{
    let battlefield: HTMLElement = createElement({ tagName: 'div', className: 'battlefield' });

    battlefield.appendChild(createFighter(leftOpponent,
                            ev => ev.stopPropagation(),
                            ev => ev.stopPropagation()));

    battlefield.appendChild(createFighter(rightOpponent,
                                ev => ev.stopPropagation(),
                                ev => ev.stopPropagation()));
    
    return battlefield;
}

