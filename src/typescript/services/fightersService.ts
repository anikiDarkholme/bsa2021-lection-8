import { callApi } from '../helpers/apiHelper';
import { FighterInfo } from '../models/FighterInfo';
import { FullFighterInfo } from '../models/FullFighterInfo';

export async function getFighters(): Promise<FighterInfo[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET') as FighterInfo[];
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: number): Promise<FullFighterInfo> {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET') as FullFighterInfo;
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

