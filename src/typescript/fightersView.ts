import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { FighterInfo } from './models/FighterInfo';
import { FullFighterInfo } from './models/FullFighterInfo';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters: FighterInfo[]): HTMLElement {
  const selectFighterForBattle = createFightersSelector();

  const fighterElements = fighters.map(fighter => 
        createFighter(fighter,
             (ev: Event) => showFighterDetails(ev, fighter),
             (ev: Event) => selectFighterForBattle(ev, fighter)));

  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache: Map<number, FullFighterInfo> = new Map<number, FullFighterInfo>();

async function showFighterDetails(ev: Event, fighter: FighterInfo) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: number): Promise<FullFighterInfo> 
{
  if(fightersDetailsCache.has(fighterId))
    return fightersDetailsCache.get(fighterId)!;

    let fighterDetails = await getFighterDetails(fighterId);

    fightersDetailsCache.set(fighterId, fighterDetails);

    return fighterDetails;
}

function createFightersSelector()
: (ev: Event, fighter: FighterInfo) => Promise<void> 
{
  const selectedFighters = new Map<number, FullFighterInfo>();

  return async function selectFighterForBattle(event: Event, fighter: FighterInfo) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      let competitors: FullFighterInfo[] = Array.from(selectedFighters.values());
      
      ShowFight(competitors);
      cleanMap(selectedFighters);

    }
  }
}

function ShowFight(selectedFighters: FullFighterInfo[]) 
{ 
   let firstFighter = selectedFighters[0];
   let secondFighter = selectedFighters[1]

   fight(firstFighter, secondFighter);
  
  //showWinnerModal(winner);

}

function cleanMap(selectedFighters: Map<number, FullFighterInfo>) {
  selectedFighters.clear();

  document.querySelectorAll("input[type = 'checkbox']")
    .forEach(x => (x as HTMLInputElement).checked = false);
}

