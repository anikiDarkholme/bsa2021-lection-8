import { IElement } from '../models/IElement';

export function createElement(elementInfo: IElement) {

    let {tagName, className, attributes = {}} = elementInfo;

    const element = document.createElement(tagName);
    
    if (className) {
      element.classList.add(className);
    }
  
    for (let i: number = 0; i < Object.keys(attributes).length; i++){
      element.setAttribute(Object.keys(attributes)[i], Object.values(attributes)[i]);
    }
    
    return element;
  }