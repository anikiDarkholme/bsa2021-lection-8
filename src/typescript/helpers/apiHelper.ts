import { fightersDetails, fighters } from './mockData';
import { API_URL } from '../constants/apiURL';
import { FighterInfo } from '../models/FighterInfo';
import { FullFighterInfo } from '../models/FullFighterInfo';

const useMockAPI = true;

async function callApi(endpoint: string, method: string):
Promise<FighterInfo[] | FullFighterInfo>
{
  const url: string = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string):
Promise<FighterInfo[] | FullFighterInfo>
{
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string):
FullFighterInfo {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = Number.parseInt(endpoint.substring(start + 1, end));

  return fightersDetails.filter((it) => it._id === id)[0];
}

export { callApi };
