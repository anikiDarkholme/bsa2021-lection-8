import { getFighters } from './services/fightersService'
import { createElement } from './helpers/domHelper';
import { createFighters } from './fightersView';
import { FighterInfo } from './models/FighterInfo';

const rootElement: HTMLElement | null = 
    document.getElementById('root')?
    document.getElementById('root') :
    createElement({tagName : 'div', attributes: {'id': 'root'}});

const loadingElement: HTMLElement | null = 
    document.getElementById('loading-overlay') ?
    document.getElementById('loading-overlay') :
    createElement({tagName : 'div', attributes: {'id': 'loading-overlay'}});
    

export async function startApp() {
  try {
      if(loadingElement)
    loadingElement.style.visibility = 'visible';
    
    const fighters: FighterInfo[] = 
        await getFighters();

    const fightersElement: HTMLElement =
        createFighters(fighters);

    if(rootElement)
    rootElement.appendChild(fightersElement);
  } 
  catch (error) 
  {
    console.warn(error);
    
    if(rootElement)
    rootElement.innerText = 'Failed to load data';
  } 
  finally 
  {
      if(loadingElement)
    loadingElement.style.visibility = 'hidden';
  }
}
