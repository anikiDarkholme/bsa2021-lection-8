import { FighterInfo } from "./FighterInfo";

export class FullFighterInfo extends FighterInfo{
    health: number;
    attack: number;
    defense: number;
}