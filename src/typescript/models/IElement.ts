export interface IElement{
    tagName: string;
    className?: string;
    attributes?: object
}
