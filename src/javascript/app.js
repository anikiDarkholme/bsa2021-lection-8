import { getFighters } from './services/fightersService';
import { createElement } from './helpers/domHelper';
import { createFighters } from './fightersView';
const rootElement = document.getElementById('root') ?
    document.getElementById('root') :
    createElement({ tagName: 'div', attributes: { 'id': 'root' } });
const loadingElement = document.getElementById('loading-overlay') ?
    document.getElementById('loading-overlay') :
    createElement({ tagName: 'div', attributes: { 'id': 'loading-overlay' } });
export async function startApp() {
    try {
        if (loadingElement)
            loadingElement.style.visibility = 'visible';
        const fighters = await getFighters();
        const fightersElement = createFighters(fighters);
        if (rootElement)
            rootElement.appendChild(fightersElement);
    }
    catch (error) {
        console.warn(error);
        if (rootElement)
            rootElement.innerText = 'Failed to load data';
    }
    finally {
        if (loadingElement)
            loadingElement.style.visibility = 'hidden';
    }
}
//# sourceMappingURL=app.js.map