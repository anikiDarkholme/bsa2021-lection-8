import { createFighter } from "../fighterView";
import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";
export function showFightScene(leftOpponent, rightOpponent) {
    let battlefield = createBattlefield(leftOpponent, rightOpponent);
    showModal({
        title: `Find between ${leftOpponent.name} and ${rightOpponent.name}`,
        bodyElement: battlefield
    });
}
function createBattlefield(leftOpponent, rightOpponent) {
    let battlefield = createElement({ tagName: 'div', className: 'battlefield' });
    battlefield.appendChild(createFighter(leftOpponent, ev => ev.stopPropagation(), ev => ev.stopPropagation()));
    battlefield.appendChild(createFighter(rightOpponent, ev => ev.stopPropagation(), ev => ev.stopPropagation()));
    return battlefield;
}
//# sourceMappingURL=fightScene.js.map