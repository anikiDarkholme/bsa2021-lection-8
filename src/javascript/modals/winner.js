import { createFighter } from "../fighterView";
import { showModal } from "./modal";
export function showWinnerModal(fighter) {
    let fighterElement = createFighter(fighter, (ev) => ev.stopPropagation(), (ev) => ev.stopPropagation());
    showModal({ title: `Winner : `, bodyElement: fighterElement });
}
//# sourceMappingURL=winner.js.map