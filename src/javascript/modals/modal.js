import { createElement } from '../helpers/domHelper';
export function showModal(element) {
    const root = getModalContainer();
    const modal = createModal(element);
    if (root) {
        root.append(modal);
    }
    return Number.parseInt(modal.getAttribute("id"));
}
function getModalContainer() {
    return document.getElementById('root');
}
function createModal(element) {
    let { title, bodyElement } = element;
    const layer = createElement({ tagName: 'div', className: 'modal-layer', attributes: { 'id': makeid(5) } });
    const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
    const header = createHeader(title);
    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);
    return layer;
}
function createHeader(title) {
    const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
    const titleElement = createElement({ tagName: 'span' });
    const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
    titleElement.innerText = title;
    closeButton.innerText = '×';
    closeButton.addEventListener('click', hideModal);
    headerElement.append(title, closeButton);
    return headerElement;
}
function hideModal(ev) {
    const modal = this.parentElement?.parentElement?.parentElement;
    modal?.remove();
    var id = window.setTimeout(function () { }, 0);
    while (id--) {
        window.clearTimeout(id);
    }
}
function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}
//# sourceMappingURL=modal.js.map