import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { getFighterDetails } from './services/fightersService';
export function createFighters(fighters) {
    const selectFighterForBattle = createFightersSelector();
    const fighterElements = fighters.map(fighter => createFighter(fighter, (ev) => showFighterDetails(ev, fighter), (ev) => selectFighterForBattle(ev, fighter)));
    const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });
    fightersContainer.append(...fighterElements);
    return fightersContainer;
}
const fightersDetailsCache = new Map();
async function showFighterDetails(ev, fighter) {
    const fullInfo = await getFighterInfo(fighter._id);
    showFighterDetailsModal(fullInfo);
}
export async function getFighterInfo(fighterId) {
    if (fightersDetailsCache.has(fighterId))
        return fightersDetailsCache.get(fighterId);
    let fighterDetails = await getFighterDetails(fighterId);
    fightersDetailsCache.set(fighterId, fighterDetails);
    return fighterDetails;
}
function createFightersSelector() {
    const selectedFighters = new Map();
    return async function selectFighterForBattle(event, fighter) {
        const fullInfo = await getFighterInfo(fighter._id);
        if (event.target.checked) {
            selectedFighters.set(fighter._id, fullInfo);
        }
        else {
            selectedFighters.delete(fighter._id);
        }
        if (selectedFighters.size === 2) {
            let competitors = Array.from(selectedFighters.values());
            ShowFight(competitors);
            cleanMap(selectedFighters);
        }
    };
}
function ShowFight(selectedFighters) {
    let firstFighter = selectedFighters[0];
    let secondFighter = selectedFighters[1];
    fight(firstFighter, secondFighter);
}
function cleanMap(selectedFighters) {
    selectedFighters.clear();
    document.querySelectorAll("input[type = 'checkbox']")
        .forEach(x => x.checked = false);
}
//# sourceMappingURL=fightersView.js.map