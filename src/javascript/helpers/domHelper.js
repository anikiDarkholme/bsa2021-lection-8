export function createElement(elementInfo) {
    let { tagName, className, attributes = {} } = elementInfo;
    const element = document.createElement(tagName);
    if (className) {
        element.classList.add(className);
    }
    for (let i = 0; i < Object.keys(attributes).length; i++) {
        element.setAttribute(Object.keys(attributes)[i], Object.values(attributes)[i]);
    }
    return element;
}
//# sourceMappingURL=domHelper.js.map